from setuptools import setup

setup(
    name='tagcounter',
    version='1.0',
    author='Viktar Smaliuk',
    packages=['tagcounter'],
    description='Counts the number of HTML tag for given URL',
    package_data={'': ['*.yaml']},
    install_requires=['requests','bs4','argparse','pyyaml'],
)
