from wu.web_utils import get_from_web, get_from_cache, get_force, log_url
# TODO : Import, Setup package
import argparse

from wu.yam_dict import YamlDict as YamlD

if __name__ == "__main__":
    """"""

    yd = YamlD()

    parser = argparse.ArgumentParser(description="TagCounter calculates the number of HTML tags for the given URL "
                                                 "address (or a macro)",
                                     epilog="Greetings to Konstantin Ilyashenko")

    group = parser.add_mutually_exclusive_group()
    #  TODO : sort params
    group.add_argument("-g", "--get", type=str, metavar="URL", help="get number of tags from web site")
    group.add_argument("-c", "--cache", type=str, metavar="URL", help="get number of tags from database")
    group.add_argument("-f", "--force", type=str, metavar="URL", help="get data from web if not found in database")
    group.add_argument("-s", "--show", help="show alias list", action='store_true')
    group.add_argument("-a", "--add", type=str, metavar="alias:url", help="add new alias to dict")
    group.add_argument("-d", "--delete", type=str, metavar="alias", help="delete given alias from dict")
    group.add_argument("-r", "--reset", help="regenerate alias file", action='store_true')
    # TODO : --log [tail]
    group.add_argument("-l", "--log", help="view log details", action='store_true')

    args = parser.parse_args()

    # TODO : use custom Actions objects?
    # wu.parse_alias()       +++++__
    # wu.validate url        +++++__
    # YAML                   ++++++_

    # LogU Init             ______
    # LogU Add               ______
    # LogU Show [tail]       ______

    # WebU Add               ++++++
    # WebU Get               +++++_
    # WebU Force             +++++_

    # DbU Init              +++++_
    # DbU Add                +++++_
    # DbU Get                +++++_

    # TODO : important
    # UT                     ______

    # AlU Show               +++++_
    # AlU Add                +++++_
    # AlU Delete             +++++_

    if args.get:
        # print(f"Get url '{args.get}' from web")

        url = yd.validate_alias(args.get)
        log_url(url)
        res = get_from_web(url)

        yd.print_result(res)

    elif args.cache:
        # print(f"get cache {args.cache}")

        url = yd.validate_alias(args.cache)
        res = get_from_cache(url)
        yd.print_result(res)

    elif args.force:
        # print(f"force {args.force}")

        url = yd.validate_alias(args.force)

        res = get_force(url)
        yd.print_result(res)

    elif args.add:
        # print(f"add alias {args.add}")

        alias, url = args.add.split(":")
        yd.add(alias, url)
        yd.save()

    elif args.delete:
        # print(f"delete alias {args.delete}")

        yd.delete(args.delete)
        yd.save()

    elif args.show:
        # print(f"show aliases {args.show}")

        yd.show()

    elif args.log:
        print(f"show log {args.log}")

    elif args.reset:
        print(f"Reset aliases file")

        if 'y' == input("Are You sure? (Y/N):")[0].lower:
            pass
        yd.reset()

    else:

        from gui.gui import GuiTagcounter

        view = GuiTagcounter()
        view.show()
