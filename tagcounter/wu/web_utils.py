# TODO : required
from urllib.parse import urlparse
# TODO : required
import requests
# TODO : required
from bs4 import BeautifulSoup

from datetime import datetime

from wu.sql_utils import sql_fin, sql_add


def validate(siteurl):
    if siteurl[:6] != 'http://' and siteurl[:8] != 'https://':
        return 'http://' + siteurl
    else:
        return None


def get_from_web(params):  # TODO : service
    """
    http.client
    """
    if not params:
        return

    r = requests.get(params)

    soup = BeautifulSoup(r.content, features="html.parser")

    tag_counter_dict = dict()

    for tag in soup.find_all(True):
        tag_counter_dict[tag.name] = tag_counter_dict.setdefault(tag.name, 0) + 1

    url_obj = urlparse(params)
    sitename = url_obj.netloc.split('.')[-2]

    sl = sorted(tag_counter_dict.items(), key=lambda item: item[1], reverse=True)

    date_now = datetime.now().strftime("%b %d %Y %H:%M:%S")
    sql_add(sitename, url_obj.netloc, date_now, sl)

    return [[sitename, url_obj.netloc, date_now, sl]]

    pass


def get_from_cache(params):

    url_obj = urlparse(params)

    sitename = url_obj.netloc.split('.')[-2]

    result = sql_fin(sitename)

    return result


def get_force(params):
    print(f"get force {params}")
    result = get_from_cache(params)

    if not result:
        result = get_from_web(params)

    return result


def log_url(params):
    with open('tagc.log', "a") as f:
        date_now = datetime.now().strftime("%b %d %Y %H:%M:%S")
        print(date_now, params)
