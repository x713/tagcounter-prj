import sqlite3
import pickle
# TODO : required

sqlite_create_table_query = '''CREATE TABLE IF NOT EXISTS sqlitedb_tagcounter (
                               sitename TEXT NOT NULL,
                               url TEXT NOT NULL,
                               scan_date datetime,
                               context BLOB
                               );'''

sqlite_version_query = "select sqlite_version();"
sqlite_select_query = "SELECT * FROM sqlitedb_tagcounter"
sqlite_delete_table_query = "DROP TABLE IF EXISTS sqlitedb_tagcounter"


def sql_add(sitename, url, scan_date, context):

    try:
        sqlite_connection = sqlite3.connect('sqlite_tagcounter.db')

        cursor = sqlite_connection.cursor()
        # print("База данных создана и успешно подключена к SQLite")

        # cursor.execute(sqlite_version_query)
        # record = cursor.fetchall()
        # print("Версия базы данных SQLite: ", record)

        cursor.execute(sqlite_create_table_query)
        sqlite_connection.commit()
        # print("Таблица SQLite создана")

        blob = pickle.dumps(context)

        sqlite_insert_query = "INSERT INTO sqlitedb_tagcounter VALUES( ?, ?, ?, ?);"
        sqlite_insert_values = [sitename, url, scan_date, blob]

        cursor.execute(sqlite_insert_query, sqlite_insert_values)
        sqlite_connection.commit()

        cursor.close()

    except sqlite3.Error as error:
        print("Ошибка при подключении к sqlite", error)
    finally:
        if sqlite_connection:
            sqlite_connection.close()
            # print("Соединение с SQLite закрыто")

def sql_get():
    try:
        sqlite_connection = sqlite3.connect('sqlite_tagcounter.db')
        cursor = sqlite_connection.cursor()

        cursor.execute(sqlite_select_query)

        result = cursor.fetchall()

        cursor.close()

        return result

    except sqlite3.Error as error:
        print("Ошибка при подключении к sqlite", error)
    finally:
        if sqlite_connection:
            sqlite_connection.close()
            # print("Соединение с SQLite закрыто")

def sql_fin(name):

    try:
        sqlite_connection = sqlite3.connect('sqlite_tagcounter.db')
        cursor = sqlite_connection.cursor()

        sqlite_find_query = f"SELECT * FROM sqlitedb_tagcounter WHERE sitename = '{name}'"

        cursor.execute(sqlite_find_query)

        result = cursor.fetchall()

        result_unpack = []
        for row in result:
            sp = pickle.loads(row[3])
            row_list = [row[i] for i in range(3)]
            row_list.append(sp)
            result_unpack.append(row_list)

        cursor.close()

        return result_unpack

    except sqlite3.Error as error:
        print("Ошибка при подключении к sqlite", error)
    finally:
        if sqlite_connection:
            sqlite_connection.close()
            # print("Соединение с SQLite закрыто")
