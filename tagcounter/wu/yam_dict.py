# TODO : pip install pyyaml
import yaml

from wu.web_utils import validate


class YamlDict:

    def __init__(self):
        self.alias_list = None
        self.init()

    def show(self):
        if self.alias_list:
            for i in self.alias_list.items():
                print(i)

    def save(self):
        to_yaml = self.alias_list

        with open('tagc_alias.yaml', 'w') as f:
            yaml.dump(to_yaml, f, default_flow_style=False)

        pass

    def reset(self):
        self.alias_list = {'ggl': 'google.com', 'ydx': 'yandex.ru', 'tut': 'tut.by'}

        self.save()

        return self.alias_list

    def init(self):

        try:
            with open('tagc_alias.yaml') as f:
                self.alias_list = yaml.safe_load(f.read())
        except:
            print("ERROR: Aliases file corrupted!")
            print("Create new aliases file")
            if input("Are You sure? (Y/N):")[0].lower == 'y':
                self.reset()
                self.init()

        return self.alias_list

    def add(self, alias, url):
        if self.alias_list.get(alias):
            print(f"Alias already exists : {alias}:{self.alias_list[alias]}")
        else:
            self.alias_list[alias] = url
            self.show()

        return self.alias_list

    def delete(self, alias):
        if self.alias_list.get(alias):
            del self.alias_list[alias]
            self.show()
        else:
            print(f"Alias doesn't exists : {alias}")

        return self.alias_list

    def print_result(self, res):
        for entry in res:
            print(entry)

    def validate_alias(self, alias):
        if self.alias_list.get(alias):
            return validate(self.alias_list[alias])
        else:
            return validate(alias)
