import tkinter as tk
# import tkinter.font as tkFont
from tkinter import scrolledtext

from wu.web_utils import get_from_cache, get_from_web, log_url
from wu.yam_dict import YamlDict as YamlD

class GuiTagcounter:
    def __init__(self):

        self.yd = YamlD()
        pass

    def show(self):

        def print_result(result):

            txt_tags.delete('0.0', tk.END)

            if result:
                for row in result:
                    for column in row:
                        txt_tags.insert(tk.INSERT, column)
                        txt_tags.insert(tk.INSERT, " ")

                    txt_tags.insert(tk.INSERT, "\n")

        def search(event):

            url = self.yd.validate_alias(txt_url.get())

            result = get_from_cache(url)

            if not result:
                result = get_from_web(url)
                log_url(url)

            if result:
                print_result(result)

        def clicked_search():
            lbl_status['text'] = "Loading web page..."
            site_url = txt_url.get()

            result = None

            if site_url:
                url = self.yd.validate_alias(txt_url.get())
                result = get_from_web(url)
                log_url(url)

            lbl_status['text'] = "Loading web page done."

            if result:
                print_result(result)

                return True

            pass

        def clicked_show():
            lbl_status['text'] = "Searching web page..."
            site_url = txt_url.get()

            if site_url:
                # TODO : validate
                url = self.yd.validate_alias(site_url)

                result = get_from_cache(url)

                lbl_status['text'] = "Searching web page done."

                if result:
                    print_result(result)

                    return True

            pass

        window = tk.Tk()
        window.title("Welcome to tagcounter")
        window.geometry('640x480')

        lbl = tk.Label(window, text="Enter site URL")
        lbl.grid(column=0, row=0, sticky=tk.W)

        txt_url = tk.Entry(window)
        txt_url.grid(column=0, row=1, columnspan=2, sticky=tk.W+tk.E)

        txt_url.bind('<Return>', search)

        btn_search = tk.Button(window, text="Загрузить", command=clicked_search)
        btn_search.grid(column=0, row=2)

        btn_show = tk.Button(window, text="Показать из базы", command=clicked_show)
        btn_show.grid(column=1, row=2)

        # TODO : Button Clean DB
        # def clicked_db_reset():
        #    pass

        # btn_show = tk.Button(window, text="Очистить базу", command=clicked_db_reset)
        # btn_show.grid(column=2, row=2)

        lbl_tags = tk.Label(window, text="Tags:")
        lbl_tags.grid(column=0, row=3)

        txt_tags = tk.scrolledtext.ScrolledText(window)
        txt_tags.grid(column=0, row=4, sticky=tk.N+tk.S+tk.W+tk.E, columnspan=2)

        # TODO : fun, remove
        # fn = tkFont.Font(family='futurama alien alphabet one')
        #
        # lbl_status= tk.Label(window,
        #                     text="Enter site URL",
        #                     font=fn)
        # end_fun
        lbl_status = tk.Label(window, text="Enter site URL")
        lbl_status.grid(column=0, row=5, sticky=tk.W+tk.S)

        txt_url.focus()

        window.mainloop()
